package main.java.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExerciseEightTest {

    @Test
    public void getDistanciaCTestOne() {
        // arrange
        double distanciaA = 60;
        double distanciaB = 40;
        double angulo = 60;
        double expected = 52.92;

        // act
        double result = ExerciseEight.getDistanciaC(distanciaA, distanciaB, angulo);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getDistanciaCTestTwo() {
        // arrange
        double distanciaA = 30;
        double distanciaB = 20;
        double angulo = 30;
        double expected = 16.15;

        // act
        double result = ExerciseEight.getDistanciaC(distanciaA, distanciaB, angulo);

        // assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getDistanciaCTestThree() {
        // arrange
        double distanciaA = 50;
        double distanciaB = 50;
        double angulo = 0;
        double expected = 0;

        // act
        double result = ExerciseEight.getDistanciaC(distanciaA, distanciaB, angulo);

        // assert
        assertEquals(expected, result, 0.01);
    }

}