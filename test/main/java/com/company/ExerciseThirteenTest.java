package main.java.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExerciseThirteenTest {

    @Test
    public void getMinutosDecorridosTestOne() {
        // arrange
        int horas = 18;
        int minutos = 13;
        int expected = 1093;

        // act
        int result = ExerciseThirteen.getMinutosDecorridos(horas, minutos);

        // assert
        assertEquals(result, expected, 0.1);

    }

    @Test
    public void getMinutosDecorridosTestTwo() {
        // arrange
        int horas = 0;
        int minutos = 1;
        int expected = 1;

        // act
        int result = ExerciseThirteen.getMinutosDecorridos(horas, minutos);

        // assert
        assertEquals(result, expected, 0.1);

    }

    @Test
    public void getMinutosDecorridosTestThree() {
        // arrange
        int horas = 24;
        int minutos = 0;
        int expected = 1440;

        // act
        int result = ExerciseThirteen.getMinutosDecorridos(horas, minutos);

        // assert
        assertEquals(result, expected, 0.1);

    }
}