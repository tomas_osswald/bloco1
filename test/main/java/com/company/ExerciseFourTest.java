package main.java.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExerciseFourTest {

    @Test
    public void getDistanciaTestOne() {

        double expected = 4998;
        double result = ExerciseFour.getDistancia(14.7);
        assertEquals(expected, result, 0.01);

    }

    @Test
    public void getDistanciaTestTwo() {

        double expected = 3400;
        double result = ExerciseFour.getDistancia(10);
        assertEquals(expected, result, 0.01);

    }

    @Test
    public void getDistanciaTestThree() {

        double expected = 0;
        double result = ExerciseFour.getDistancia(0);
        assertEquals(expected, result, 0.01);

    }
}