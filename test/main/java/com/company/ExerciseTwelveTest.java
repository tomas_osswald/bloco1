package main.java.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExerciseTwelveTest {

    @Test
    public void getFahrenheitTestOne() {
        // arrange
        double celsius = 38;
        double expected = 100.4;

        // act
        double result = ExerciseTwelve.getFahrenheit(celsius);

        // assert
        assertEquals(result, expected, 0.01);

    }

    @Test
    public void getFahrenheitTestTwo() {
        // arrange
        double celsius = 0;
        double expected = 32;

        // act
        double result = ExerciseTwelve.getFahrenheit(celsius);

        // assert
        assertEquals(result, expected, 0.01);

    }

    @Test
    public void getFahrenheitTestThree() {
        // arrange
        double celsius = -17.7778;
        double expected = 0;

        // act
        double result = ExerciseTwelve.getFahrenheit(celsius);

        // assert
        assertEquals(result, expected, 0.01);

    }
}