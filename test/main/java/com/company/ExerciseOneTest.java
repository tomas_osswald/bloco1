package main.java.com.company;

import org.junit.Test;

import static org.junit.Assert.*;

public class ExerciseOneTest {

    @Test
    public void getPercentagemTestOne() {

        double expected = 60.0;
        double result = ExerciseOne.getPercentagem(18,30);
        assertEquals(expected, result, 0.01);
    }

    @Test
    public void getPercentagemTestTwo() {

        double expected = 0;
        double result = ExerciseOne.getPercentagem(0, 60);
        assertEquals(expected, result, 0.01);

    }
}