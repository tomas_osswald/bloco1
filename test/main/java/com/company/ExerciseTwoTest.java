package main.java.com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExerciseTwoTest {

    @Test
    public void getTotalTestOne() {

        double expected = 25;
        int rosas = 2, tulipas = 1;
        double precoRosas = 10, precoTulipas = 5;
        double result = ExerciseTwo.getTotal(rosas, tulipas, precoRosas, precoTulipas);
        assertEquals(expected, result, 0.001);

    }

    @Test
    public void getTotalTestTwo() {

        double expected = 0;
        int rosas = 12, tulipas = 132;
        double precoRosas = 0, precoTulipas = 0;
        double result = ExerciseTwo.getTotal(rosas, tulipas, precoRosas, precoTulipas);
        assertEquals(expected, result, 0.001);
    }
}