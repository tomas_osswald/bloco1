package main.java.com.company;

import java.util.Scanner;

public class ExerciseSix {

    public static void main(String[] args) {

        // Cálculo da altura dum edifício em metros através do Teorema de Tales
        // introdução das variáveis

        double sombraPessoa, sombraEdificio, alturaPessoa, alturaEdificio;

        // leitura das variáveis

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a sua altura em metros");
        alturaPessoa = ler.nextDouble();
        System.out.println("Introduza o comprimento da sua sombra em metros");
        sombraPessoa = ler.nextDouble();
        System.out.println("Introduza o comprimento da sombra do edifício em metros");
        sombraEdificio = ler.nextDouble();

        // cálculo

        alturaEdificio = getAlturaEdificio(sombraPessoa, sombraEdificio, alturaPessoa);

        // apresentação do resultado

        System.out.println("A altura do edifício é de " + String.format("%.2f", alturaEdificio) + " metros");

    }

    public static double getAlturaEdificio(double sombraPessoa, double sombraEdificio, double alturaPessoa) {
        return (sombraEdificio / sombraPessoa) * alturaPessoa;
    }

}
