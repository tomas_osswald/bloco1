package main.java.com.company;

import java.util.Scanner;

public class ExerciseFive {

    public static void main(String[] args) {

        // Cálculo da altura dum edifício em metros através da gravidade
        // introdução das variáveis

        double tempo, altura;

        // leitura das variáveis

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o intervalo de tempo em segundos");
        tempo = ler.nextDouble();

        // cálculo da altura

        altura = getAltura(tempo);

        // apresentação do resultado

        System.out.println("A altura do edifício é de " + String.format("%.2f", altura) + " metros");

    }

    public static double getAltura(double tempo) {
        final double GRAVIDADE = 9.8;
        return (GRAVIDADE * (tempo * tempo)) / 2;

    }

}
