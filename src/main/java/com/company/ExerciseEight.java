package main.java.com.company;

import java.util.Scanner;

public class ExerciseEight {

    public static void main(String[] args) {

        // Cálculo da distância entre dois operários com a lei dos cossenos
        // introdução das variáveis

        double distanciaA, distanciaB, angulo, distanciaC;

        // leitura das variáveis

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a distância em metros entre o primeiro operário e o ângulo");
        distanciaA = ler.nextDouble();
        System.out.println("Introduza a distância em metros entre o segundo operário e o ângulo");
        distanciaB = ler.nextDouble();
        System.out.println("Introduza o ângulo em graus");
        angulo = (ler.nextDouble());

        // cálculo

        distanciaC = getDistanciaC(distanciaA, distanciaB, angulo);

        // apresentação do resultado

        System.out.println("A distância entre os dois operários é de " + String.format("%.2f", distanciaC) + " metros");

    }

    public static double getDistanciaC(double distanciaA, double distanciaB, double angulo) {
        return Math.sqrt(Math.pow(distanciaA, 2) + Math.pow(distanciaB, 2) - 2 * (distanciaA * distanciaB) * Math.cos(Math.toRadians(angulo)));
    }

}