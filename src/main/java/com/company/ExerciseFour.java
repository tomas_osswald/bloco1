package main.java.com.company;

import java.util.Scanner;

public class ExerciseFour {

    public static void main(String[] args) {

        // Cálculo da distância duma trovoada em metros
        // introdução das variáveis

        double intervalo, distancia;

        // leitura das variáveis

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o intervalo de tempo em segundos");
        intervalo = ler.nextDouble();

        // cálculo da distância

        distancia = getDistancia(intervalo);

        // apresentação do resultado

        System.out.println("A distância da trovoada é de " + String.format("%.0f", distancia) + " metros");

    }

    public static double getDistancia(double intervalo) {
        final double VELOCIDADE_SOM = 340;
        return intervalo * VELOCIDADE_SOM;

    }

}
