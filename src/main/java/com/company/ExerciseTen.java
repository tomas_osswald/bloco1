package main.java.com.company;

import java.util.Scanner;

public class ExerciseTen {

    public static void main(String[] args) {

        // Calcular hipotenusa do triangulo
        // introducao das variaveis

        double cateto1, cateto2, hipotenusa;

        // leitura das variaveis

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o valor do lado C1 do triângulo");
        cateto1 = ler.nextDouble();
        System.out.println("Introduza o valor do lado C2 do triângulo");
        cateto2 = ler.nextDouble();

        // processamento

        hipotenusa = getHipotenusa(cateto1, cateto2);

        // apresentacao dos resultados

        System.out.println("A valor da hipotenusa do triângulo é " + String.format("%.2f", hipotenusa));

    }

    public static double getHipotenusa(double cateto1, double cateto2) {
        return Math.sqrt(Math.pow(cateto1, 2) + Math.pow(cateto2, 2));
    }

}
