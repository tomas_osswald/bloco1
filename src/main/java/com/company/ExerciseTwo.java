package main.java.com.company;

import java.util.Scanner;

public class ExerciseTwo {

    public static void main(String[] args) {

        // Cálculo do valor de um ramo de flores
        // introdução das variáveis

        int rosas, tulipas;
        double precoRosas, precoTulipas, total;

        // leitura das variáveis

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o número de rosas");
        rosas = ler.nextInt();
        System.out.println("Introduza o preço por rosa");
        precoRosas = ler.nextDouble();
        System.out.println("Introduza o número de tulipas");
        tulipas = ler.nextInt();
        System.out.println("Introduza o preço por tulipa");
        precoTulipas = ler.nextDouble();

        // cálculo do volume. Multiplicação por 1000 para converter a unidade de metros cúbicos para litros.

        total = getTotal(rosas, tulipas, precoRosas, precoTulipas);

        // apresentação do resultado

        System.out.println("O preço do ramo é de " + String.format("%.2f", total) + " Euros");

    }

    public static double getTotal(int rosas, int tulipas, double precoRosas, double precoTulipas) {
        return (rosas * precoRosas) + (tulipas * precoTulipas);
    }

}
