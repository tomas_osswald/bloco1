package main.java.com.company;

import java.util.Scanner;

public class ExerciseThree {

    public static void main(String[] args) {

        // Cálculo do volume dum vasilhame cilíndrico em litros
        // introdução das variáveis

        double raio, altura, volume;

        // leitura das variáveis

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza o raio");
        raio = ler.nextDouble();
        System.out.println("Introduza a altura");
        altura = ler.nextDouble();

        // cálculo do volume. Multiplicação por 1000 para converter a unidade de metros cúbicos para litros.

        volume = getVolume(raio, altura);

        // apresentação do resultado

        System.out.println("A capacidade do vasilhame é de " + String.format("%.2f", volume) + " litros");

    }

    public static double getVolume(double raio, double altura) {
        double CONVERTER_PARA_LITROS = 1000;
        return Math.PI * raio * raio * altura * CONVERTER_PARA_LITROS;
    }

}
