package main.java.com.company;

import java.util.Scanner;

public class ExerciseTwelve {

    public static void main(String[] args) {

        // Converter graus Celsius para graus Fahrenheit
        // introducao de variaveis

        double celsius, fahrenheit;

        // leitura das variáveis

        Scanner ler = new Scanner(System.in);

        System.out.println("Introduza a temperatura em graus Celsius");
        celsius = ler.nextDouble();

        // processamento

        fahrenheit = getFahrenheit(celsius);

        // apresentacao do resultado

        System.out.println(celsius + " graus Celsius são " + fahrenheit + " graus Fahrenheit");

    }

    public static double getFahrenheit(double celsius) {
        return (celsius * 9/5) + 32;
    }

}
